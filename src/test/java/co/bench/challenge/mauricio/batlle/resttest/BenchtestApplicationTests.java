package co.bench.challenge.mauricio.batlle.resttest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import co.bench.challenge.mauricio.batlle.resttest.origin.TransactionsOrigin;
import co.bench.challenge.mauricio.batlle.resttest.pojos.Transaction;
import co.bench.challenge.mauricio.batlle.resttest.pojos.TransactionsPage;
import co.bench.challenge.mauricio.batlle.resttest.processor.MultiPageProcess;

@SpringBootTest
class BenchtestApplicationTests
{
	@Autowired 
	MultiPageProcess process;
	@Test
	public void basicObjectCreationTest()
	{
		TransactionsOrigin origin = new TransactionsOrigin();
		TransactionsPage testObject = origin.getObjectFromWS( 1 );
		assertNotNull( testObject );
		assertEquals( testObject.getPage(), 1 );
		assertEquals( testObject.getTotalCount(), 38 );
		assertNotNull( testObject.getTransactions() );
		assertEquals( testObject.getTransactions().get( 1 ).getDate(), "2013-12-21" );
		assertEquals( testObject.getTransactions().get( 1 ).getAmount(), "-8.1" );
	}

	@Test
	public void dateFetchTest() throws ExecutionException, InterruptedException
	{
		List<Transaction> response = process.DataFetch();
		assertNotNull( response );
		assertEquals( response.size(), 38 );
		assertEquals( response.get( 1 ).getClass(), Transaction.class );
	}

	@Test
	public void ProcessDailyAmountsFailTest() throws ExecutionException, InterruptedException
	{
		assertThrows( NullPointerException.class,
				() -> {
					process.ProcessDailyAmounts( null );
				} );

		assertThrows( ArrayIndexOutOfBoundsException.class,
				() -> {
					List<Transaction> emptyTransactions = new ArrayList<Transaction>();
					process.ProcessDailyAmounts( emptyTransactions );
				} );

	}

}
