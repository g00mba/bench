package co.bench.challenge.mauricio.batlle.resttest.pojos;

import java.time.LocalDate;

import org.joda.money.Money;

public class DailyBalance
{
	public final LocalDate date;
	public final Money amount;

	public DailyBalance(LocalDate date, Money amount)
	{
		this.date = date;
		this.amount = amount;
	}

	public LocalDate getDate()
	{
		return date;
	}

	public Money getAmount()
	{
		return amount;
	}
}
