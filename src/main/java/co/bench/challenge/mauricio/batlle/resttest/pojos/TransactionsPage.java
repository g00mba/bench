
package co.bench.challenge.mauricio.batlle.resttest.pojos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude( JsonInclude.Include.NON_NULL )
@JsonPropertyOrder( {
		"totalCount",
		"page",
		"transactions"
} )
@Component
public class TransactionsPage
{

	@JsonProperty( "totalCount" )
	private Integer totalCount;
	@JsonProperty( "page" )
	private Integer page;
	@JsonProperty( "transactions" )
	private List<Transaction> transactions = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty( "totalCount" )
	public Integer getTotalCount()
	{
		return totalCount;
	}

	@JsonProperty( "totalCount" )
	public void setTotalCount( Integer totalCount )
	{
		this.totalCount = totalCount;
	}

	@JsonProperty( "page" )
	public Integer getPage()
	{
		return page;
	}

	@JsonProperty( "page" )
	public void setPage( Integer page )
	{
		this.page = page;
	}

	@JsonProperty( "transactions" )
	public List<Transaction> getTransactions()
	{
		return transactions;
	}

	@JsonProperty( "transactions" )
	public void setTransactions( List<Transaction> transactions )
	{
		this.transactions = transactions;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty( String name, Object value )
	{
		this.additionalProperties.put( name, value );
	}

}
