package co.bench.challenge.mauricio.batlle.resttest.processor;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.bench.challenge.mauricio.batlle.resttest.origin.TransactionsOrigin;
import co.bench.challenge.mauricio.batlle.resttest.pojos.DailyBalance;
import co.bench.challenge.mauricio.batlle.resttest.pojos.Transaction;

@Service
public class MultiPageProcess
{
	@Autowired
	TransactionsOrigin origin;
	
	//get me pages until I have the expected amount of transactions	
	public List<Transaction> DataFetch() 
	{
		List<Transaction> result = new LinkedList<>();
		Integer transactionCount = origin.getTransactionCount();
		Integer page = 1;
		int i = 0;
		do
		{
			result.addAll(origin.getObjectFromWS( page ).getTransactions());
			i = result.size();
			page++;
		}
		while ( i < transactionCount );
		System.out.println("final transaction count: " + i);
		return result;
	}

	public void ProcessDailyAmounts(List<Transaction> transactions)
	{
		//group the transactions per day and order them
		Map<LocalDate, Money> dailyResults =
				transactions.stream()
						.collect(
								Collectors.toMap(
										t -> LocalDate.parse( t.getDate()),
										m -> Money.parse("CAD " + m.getAmount()),
										Money::plus,
										TreeMap::new));
		System.out.println("balances per day");
		dailyResults.forEach((key, value) -> {
			System.out.println(key.toString() + ": " + value.toString());
		} );

		//Calculate running daily balances
		final DailyBalance[] balances=
				dailyResults.entrySet().stream()
						.map(e -> new DailyBalance( e.getKey(), e.getValue()))
						.toArray( DailyBalance[]::new );
		Arrays.parallelPrefix(
				balances, (acc, it) -> new DailyBalance(it.date, it.amount.plus( acc.amount )));
		System.out.println( "final balance is " + balances[balances.length - 1].getAmount());
		System.out.println("running balance");
		for (DailyBalance lt : balances)
		{
			System.out.println(lt.getDate().toString() + " " + lt.getAmount().toString());
		}
	}
}
