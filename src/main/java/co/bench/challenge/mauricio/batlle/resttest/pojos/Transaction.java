
package co.bench.challenge.mauricio.batlle.resttest.pojos;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude( JsonInclude.Include.NON_NULL )
@JsonPropertyOrder( {
		"Date",
		"Ledger",
		"Amount",
		"Company"
} )
@Component
public class Transaction
{

	@JsonProperty( "Date" )
	private String date;
	@JsonProperty( "Ledger" )
	private String ledger;
	@JsonProperty( "Amount" )
	private String amount;
	@JsonProperty( "Company" )
	private String company;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty( "Date" )
	public String getDate()
	{
		return date;
	}

	@JsonProperty( "Date" )
	public void setDate( String date )
	{
		this.date = date;
	}

	@JsonProperty( "Ledger" )
	public String getLedger()
	{
		return ledger;
	}

	@JsonProperty( "Ledger" )
	public void setLedger( String ledger )
	{
		this.ledger = ledger;
	}

	@JsonProperty( "Amount" )
	public String getAmount()
	{
		return amount;
	}

	@JsonProperty( "Amount" )
	public void setAmount( String amount )
	{
		this.amount = amount;
	}

	@JsonProperty( "Company" )
	public String getCompany()
	{
		return company;
	}

	@JsonProperty( "Company" )
	public void setCompany( String company )
	{
		this.company = company;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty( String name, Object value )
	{
		this.additionalProperties.put( name, value );
	}

}
