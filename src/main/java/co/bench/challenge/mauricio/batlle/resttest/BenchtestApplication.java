package co.bench.challenge.mauricio.batlle.resttest;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.bench.challenge.mauricio.batlle.resttest.processor.MultiPageProcess;

@SpringBootApplication
public class BenchtestApplication implements CommandLineRunner
{

	public static void main(String[] args)
	{
		SpringApplication.run(BenchtestApplication.class, args).close();
	}

	@Autowired
	MultiPageProcess process;
	
	@Override
	public void run(String... args) throws ExecutionException, InterruptedException
	{
		process.ProcessDailyAmounts(process.DataFetch());
	}

}
