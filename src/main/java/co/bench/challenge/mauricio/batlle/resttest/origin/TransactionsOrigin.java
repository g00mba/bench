package co.bench.challenge.mauricio.batlle.resttest.origin;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.bench.challenge.mauricio.batlle.resttest.pojos.TransactionsPage;
@Async
@Component
public class TransactionsOrigin {
	private static final Logger endpointLogger = LogManager.getLogger(TransactionsOrigin.class);
	@Autowired
	TransactionsPage fetchObject;

	//give me raw data
	public  ResponseEntity<String> endpointFetch(Integer page) {
		endpointLogger.info("attempting to fecth data from bench ws service for page "+page+"...");
		RestTemplate restDoc = new RestTemplate();
		ResponseEntity<String> response = null;
		try {
			String endpoint = "https://resttest.bench.co/transactions/"+page.toString()+".json";
			response = restDoc.getForEntity(endpoint, String.class);
		} catch (RestClientException e) {
			endpointLogger.fatal("failed to fetch data from bench endpoint: " + e.getMessage());
		}

		return response;

	}
	//turn raw data into a Transaction Page object
	public TransactionsPage getObjectFromWS(Integer page) {
		endpointLogger.info("attempting to parse data obtained from endpoint...");
		ObjectMapper mapper = new ObjectMapper();
		try {
			fetchObject = mapper.readValue(endpointFetch(page).getBody(), TransactionsPage.class);
		} catch (JsonParseException e) {
			endpointLogger.error("failed to parse data from JSON response: " + e.getMessage());
		} catch (JsonMappingException e) {
			endpointLogger.error("failed to map data from JSON response: " + e.getMessage());
		} catch (IOException e) {
			endpointLogger.error("failed to process data, IO Exception: " + e.getMessage());
		}

		return fetchObject;
	}
	public final Integer getTransactionCount() {
		endpointLogger.info("fetching total amount of transactions...");
		return getObjectFromWS(1).getTotalCount();
	} 

}
