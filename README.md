**Bench Rest Test**
---
this repository contains the Java solution to Bench's rest test, By Mauricio Batlle.
---

## goals

the requested goals for the test and its status are as follows:

1. Connects to a REST API documented below and fetches all pages of financial transactions. 
    **Status: completed successfully**
2. Calculates total balance and prints it to the console, where balance is the sum of all amounts in all transactions.
    **Status: completed successfully**
3. Calculates running daily balances and prints them to the console.
    **Status: completed successfully**

**Unit Tests are included**

---

## Running the project

The easiest way to startup the project is by cloing the git repository using a java IDE, compile the project and run it from the IDE as a Spring Boot app.

alternatively, it can be run manually, please use the following steps


1. Clone the repository to a local directory
2. execute **mvn clean install**
3. locate the target folder for the compilation, the generated jar should be named benchtest-0.0.1-SNAPSHOT.jar
4. execute **java -jar benchtest-0.0.1-SNAPSHOT.jar**
